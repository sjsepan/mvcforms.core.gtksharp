# readme.md - README for MvcForms.Core.GtkSharp

## Desktop GUI app demo, on Linux, in C# / DotNet[+] / GtkSharp, using VSCode / Glade

### Purpose

To illustrate a working Desktop GUI app in c# on .Net (Core). Uses GtkSharp where WinForms was being used.

![MvcForms.Core.GtkSharp.png](./MvcForms.Core.GtkSharp.png?raw=true "Screenshot")

### Usage notes

~This application uses keys in app.config to identify a settings file to automatically load, for manual processing and saving. 1) The file name is given, and the format given must be one of two that the app library Ssepan.Application.Core knows how to serialize / deserialize: json, xml. 2) The value used must correspond to the format expected by the SettingsController in Ssepan.Application.Core. It is specified with the SerializeAs property of type SerializationFormat enum. 3) The file must reside in the user's personal directory, which on Linux is /home/username. A sample file can be found in the MvcLibrary.Core project in the Sample folder.
~The automatic loading is not necessary (it is a carry-over from the original console app) and can be disabled by editing the code in InitViewModel() in MvcView.cs to remove the ViewModel.FileOpen() call.

### Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

### Instructions/example of adding GtkSharp to dotnet and creating app ui in Glade

<https://yasar-yy.medium.com/cross-platform-desktop-gui-application-using-net-core-5894b0ad89a8>

### Install package for app configuration

~in folder for project use:
dotnet add package System.Configuration.ConfigurationManager

### GUI tutorial for Gtk#/C\#

<https://zetcode.com/gui/gtksharp/>

### History

0.11:
~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.
~replace View menu with Window and sub items

0.10:
~Fix startup handling of settings filename in App.config.
~update this and dependent projects to .net8.0
~fix relative project path references in solution file for this and Ssepan.Application.Core

0.9:
~Remove ACTION_ constants moved into VM base
~move loc of viewname property
~disable call to run in load
~move some progress/stat calls into VM
~add more std menus

0.8:
~Set new Website property in AssemblyInfo (new property defined in AssemblyInfoBase).

0.7:
~Edit Glade file to fix field spacing to stop forcing wide form; change GtkBox layout and change which are using homogenous setting.
~Update screenshot

0.6:
~Changes to client app resulting from refactoring of Ssepan.Application.Core[.*].

0.5:
~Convert Ssepan.Core.*libs to use StdErr for logging, and replace System.Diagnostics.EventLogEntryType with own enum.
~Convert Mvc* projects to use log changes, passing new enum instead of one from System.Diagnostics.
~Clean up usings in all projects.

0.4:
~Convert MVCForms app and library, along with support libraries, to Core. Where WinForms was involved, convert that project to GtkSharp instead.
~Glade editor 3.22.2, Gtk 3.18

## Issues

~CS0649 apparently does not take into account [UI] attribute and how controls are wired to glade file, so ignore for now

## Contact

Steve Sepan
<sjsepan@yahoo.com>
3/7/2024
