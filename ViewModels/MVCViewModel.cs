﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.GtkSharp;
using MvcLibrary.Core;
using Gtk;

namespace MvcForms.Core.GtkSharp
{
    /// <summary>
    /// Note: this class can subclass the base without type parameters.
    /// </summary>
    public class MVCViewModel :
        FormsViewModel<string, MVCSettings, MVCModel, MvcView>
    {
        #region Constructors
        public MVCViewModel() { }//Note: not called, but need to be present to compile--SJS

        public MVCViewModel
        (
            PropertyChangedEventHandler propertyChangedEventHandlerDelegate,
            Dictionary<string, string> actionIconImages,
            FileDialogInfo<Window, ResponseType> settingsFileDialogInfo
        ) :
            base(propertyChangedEventHandlerDelegate, actionIconImages, settingsFileDialogInfo)
        {
            try
            {
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// model specific, not generic
        /// </summary>
        internal void DoSomething()
        {
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Doing something...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                ModelController<MVCModel>.Model.SomeBoolean = !ModelController<MVCModel>.Model.SomeBoolean;
				ModelController<MVCModel>.Model.SomeInt++;
                ModelController<MVCModel>.Model.SomeString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = !ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
				ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt++;
                ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = DateTime.Now.ToString();

                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = !ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
				ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt++;
                ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = DateTime.Now.ToString();
                //TODO:implement custom messages
                UpdateStatusBarMessages(null, null, DateTime.Now.ToLongTimeString());

                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar("Did something.");
            }
        }

        //override base
        public override void EditPreferences()
        {
            string errorMessage = null;
            FileDialogInfo<Window, ResponseType> fileDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Preferences" + ACTION_IN_PROGRESS,
                    null,
                    _actionIconImages["Preferences"],
                    true,
                    33
                );

                //get folder here
                fileDialogInfo = new FileDialogInfo<Window, ResponseType>
                (
                    View,
                    true,
                    "Select Folder...",
                    ResponseType.None
                );

                if (!Dialogs.GetFolderPath(ref fileDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fileDialogInfo.Response != ResponseType.None)
                {
                    if (fileDialogInfo.Response == ResponseType.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + fileDialogInfo.Filename + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if open was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                    StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    return; //if open was cancelled
                }

                //override base, which did this
                // if (!Preferences())
                // {
                //     throw new ApplicationException("'Preferences' error");
                // }

                StopProgressBar(StatusMessage);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("Preferences failed: {0}", ex.Message));
            }
        }

        /// <summary>
        /// get font name
        /// </summary>
        public void GetFont()
        {
            string errorMessage = null;
            FontDialogInfo<Window, ResponseType, Pango.FontDescription> fontDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Font...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                //Note:must be set or gets null ref error after dialog closes
                Pango.FontDescription fontDescription = new( );
                fontDialogInfo = new FontDialogInfo<Window, ResponseType, Pango.FontDescription>
                (
                    parent: View,
                    modal: true,
                    title: "Select Font",
                    response: ResponseType.None,
                    fontDescription: fontDescription
                );

                if (!Dialogs.GetFont(ref fontDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (fontDialogInfo.Response != ResponseType.None)
                {
                    if (fontDialogInfo.Response == ResponseType.Ok)
                    {
                        UpdateStatusBarMessages(StatusMessage + fontDialogInfo.FontDescription + ACTION_IN_PROGRESS, null);
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }

        /// <summary>
        /// get color RGB
        /// </summary>
        public void GetColor()
        {
            string errorMessage = null;
            ColorDialogInfo<Window, ResponseType, Gdk.RGBA> colorDialogInfo = null;
            StatusMessage = string.Empty;
            ErrorMessage = string.Empty;

            try
            {
                StartProgressBar
                (
                    "Get Color...",
                    null,
                    null, //_actionIconImages["Xxx"],
                    true,
                    33
                );

                Gdk.RGBA color = Gdk.RGBA.Zero;
                colorDialogInfo = new ColorDialogInfo<Window, ResponseType, Gdk.RGBA>
                (
                    View,
                    true,
                    "Select Color",
                    ResponseType.None,
                    color
                );

                if (!Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("GetFolderPath: {0}", errorMessage));
                }

                if (colorDialogInfo.Response != ResponseType.None)
                {
                    if (colorDialogInfo.Response == ResponseType.Ok)
                    {
                        UpdateStatusBarMessages
                        (
                            StatusMessage +
                            string.Format
                            (
                                "Red:{0},Green:{1},Blue:{2}",
                                colorDialogInfo.Color.Red.ToString(),
                                colorDialogInfo.Color.Green.ToString(),
                                colorDialogInfo.Color.Blue.ToString()
                            ) +
                            ACTION_IN_PROGRESS,
                            null
                        );
                    }
                    else
                    {
                        //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                    }
                }
                else
                {
                    //treat as cancel
                        StopProgressBar(StatusMessage + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                        return; //if dialog was cancelled
                }
                // if (Dialogs.GetColor(ref colorDialogInfo, ref errorMessage))
                // {
                //     if (colorDialogInfo.Response == ResponseType.Ok)
                //     {
                //         StatusMessage += 
                //             string.Format
                //             (
                //                 "Red:{0},Green:{1},Blue:{2}", 
                //                 colorDialogInfo.Color.Red.ToString(),
                //                 colorDialogInfo.Color.Green.ToString(),
                //                 colorDialogInfo.Color.Blue.ToString()
                //             );
                //     }
                //     else
                //     {
                //         StatusMessage += "cancelled";
                //     }
                // }
                // else
                // {
                //     ErrorMessage = errorMessage;
                // }

                StopProgressBar(StatusMessage + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                StopProgressBar(null, string.Format("{0}", ex.Message));
            }
            finally
            {
                StopProgressBar(StatusMessage);
            }
        }
        #endregion Methods

    }
}
