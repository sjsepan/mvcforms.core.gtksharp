using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Ssepan.Utility.Core;
using Ssepan.Io.Core;
using Ssepan.Application.Core;
using Ssepan.Application.Core.GtkSharp;
using MvcLibrary.Core;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace MvcForms.Core.GtkSharp
{
	/// <summary>
	/// This is the View.
	/// Can base window on Window or its subclass ApplicationWindow
	/// </summary>
	public class MvcView :
        Window,//ApplicationWindow
        INotifyPropertyChanged
    {
        #region Declarations
        protected bool disposed;
        // private bool _ValueChangedProgrammatically;

        //cancellation hook
        // System.Action cancelDelegate = null;
        protected MVCViewModel ViewModel;

        #region Controls
        [UI] private readonly ImageMenuItem _menuFileNew = null; //CS0649 apparently does not take into account [UI] attribute and how controls are wired to glade file
        [UI] private readonly ImageMenuItem _menuFileOpen = null;
        [UI] private readonly ImageMenuItem _menuFileSave = null;
        [UI] private readonly ImageMenuItem _menuFileSaveAs = null;
        [UI] private readonly ImageMenuItem _menuFilePrint = null;
        [UI] private readonly ImageMenuItem _menuFilePrintPreview = null;
        [UI] private readonly ImageMenuItem _menuFileQuit = null;
        [UI] private readonly ImageMenuItem _menuEditUndo = null;
        [UI] private readonly ImageMenuItem _menuEditRedo = null;
        [UI] private readonly ImageMenuItem _menuEditSelectAll = null;
        [UI] private readonly ImageMenuItem _menuEditCut = null;
        [UI] private readonly ImageMenuItem _menuEditCopy = null;
        [UI] private readonly ImageMenuItem _menuEditPaste = null;
        [UI] private readonly MenuItem _menuEditPasteSpecial = null;
        [UI] private readonly ImageMenuItem _menuEditDelete = null;
        [UI] private readonly ImageMenuItem _menuEditFind = null;
        [UI] private readonly ImageMenuItem _menuEditReplace = null;
        [UI] private readonly ImageMenuItem _menuEditRefresh = null;
        [UI] private readonly MenuItem _menuEditProperties = null;
        [UI] private readonly ImageMenuItem _menuEditPreferences = null;
        [UI] private readonly MenuItem _menuWindowNewWindow = null;
        [UI] private readonly MenuItem _menuWindowTile = null;
        [UI] private readonly MenuItem _menuWindowCascade = null;
        [UI] private readonly MenuItem _menuWindowArrangeAll = null;
        [UI] private readonly MenuItem _menuWindowHide = null;
        [UI] private readonly MenuItem _menuWindowShow = null;
        [UI] private readonly ImageMenuItem _menuHelpContents = null;
        [UI] private readonly MenuItem _menuHelpIndex = null;
        [UI] private readonly MenuItem _menuHelpOnlineHelp = null;
        [UI] private readonly MenuItem _menuHelpLicenseInformation = null;
        [UI] private readonly MenuItem _menuHelpCheckForUpdates = null;
        [UI] private readonly ImageMenuItem _menuHelpAbout = null;

        [UI] private readonly ToolButton _tbFileNew = null;
        [UI] private readonly ToolButton _tbFileOpen = null;
        [UI] private readonly ToolButton _tbFileSave = null;
        [UI] private readonly ToolButton _tbFileSaveAs = null;
        [UI] private readonly ToolButton _tbFilePrint = null;
        [UI] private readonly ToolButton _tbEditUndo = null;
        [UI] private readonly ToolButton _tbEditRedo = null;
        [UI] private readonly ToolButton _tbEditCut = null;
        [UI] private readonly ToolButton _tbEditCopy = null;
        [UI] private readonly ToolButton _tbEditPaste = null;
        [UI] private readonly ToolButton _tbEditDelete = null;
        [UI] private readonly ToolButton _tbEditFind = null;
        [UI] private readonly ToolButton _tbEditReplace = null;
        [UI] private readonly ToolButton _tbEditRefresh = null;
        [UI] private readonly ToolButton _tbEditPreferences = null;
        [UI] private readonly ToolButton _tbHelpContents = null;

        [UI] private readonly Label _statusMessage = null;
        [UI] private readonly Label _errorMessage = null;
        [UI] private readonly ProgressBar _progressBar = null;
        [UI] private readonly Image _pictureAction = null;
        [UI] private readonly Image _pictureDirty = null;

        [UI] private readonly Button _buttonColor = null;
        [UI] private readonly Button _buttonFont = null;
        //NOTE:unless these labels are going to be accessed/modified, their declaration is not necessary, = null 
        // and will trigger a 'warning CS0414: The field 'MvcView._lblSomeInt' is assigned but its value is never used = null'
        // [UI] private Label _lblSomeInt = null;
        // [UI] private Label _lblSomeOtherInt = null;
        // [UI] private Label _lblStillAnotherInt = null;
        // [UI] private Label _lblSomeString = null;
        // [UI] private Label _lblSomeOtherString = null;
        // [UI] private Label _lblStillAnotherString = null;
        // [UI] private Label _lblSomeBoolean = null;
        // [UI] private Label _lblSomeOtherBoolean = null;
        // [UI] private Label _lblStillAnotherBoolean = null;
        [UI] private readonly Entry _txtSomeInt = null;
        [UI] private readonly Entry _txtSomeOtherInt = null;
        [UI] private readonly Entry _txtStillAnotherInt = null;
        [UI] private readonly Entry _txtSomeString = null;
        [UI] private readonly Entry _txtSomeOtherString = null;
        [UI] private readonly Entry _txtStillAnotherString = null;
        [UI] private readonly CheckButton _chkSomeBoolean = null;
        [UI] private readonly CheckButton _chkSomeOtherBoolean = null;
        [UI] private readonly CheckButton _chkStillAnotherBoolean = null;
        [UI] private readonly Button _cmdRun = null;
        #endregion Controls

        #endregion Declarations

        #region Constructors    
        public MvcView() :
            this(new Builder("MvcView.glade")) { }

        private MvcView(Builder builder) :
            base(builder.GetRawOwnedObject("MvcView"))
        {
            try
            {
                builder.Autoconnect(this);
                #region Events
                Realized += Window_Realized;
                DeleteEvent += Window_DeleteEvent;
                KeyPressEvent += Window_KeyPressEvent;

                _menuFileNew.Activated += MenuFileNew_Activated;
                _menuFileOpen.Activated += MenuFileOpen_Activated;
                _menuFileSave.Activated += MenuFileSave_Activated;
                _menuFileSaveAs.Activated += MenuFileSaveAs_Activated;
                _menuFilePrint.Activated += MenuFilePrintActivated;
                _menuFilePrintPreview.Activated += MenuFilePrintPreview_Activated;
                _menuFileQuit.Activated += MenuFileQuit_Activated;
                _menuEditUndo.Activated += MenuEditUndo_Activated;
                _menuEditRedo.Activated += MenuEditRedo_Activated;
                _menuEditSelectAll.Activated += MenuEditSelectAll_Activated;
                _menuEditCut.Activated += MenuEditCut_Activated;
                _menuEditCopy.Activated += MenuEditCopyActivated;
                _menuEditPaste.Activated += MenuEditPaste_Activated;
                _menuEditPasteSpecial.Activated += MenuEditPasteSpecial_Activated;
                _menuEditDelete.Activated += MenuEditDelete_Activated;
                _menuEditFind.Activated += MenuEditFind_Activated;
                _menuEditReplace.Activated += MenuEditReplace_Activated;
                _menuEditRefresh.Activated += MenuEditRefresh_Activated;
                _menuEditProperties.Activated += MenuEditProperties_Activated;
                _menuEditPreferences.Activated += MenuEditPreferences_Activated;
                _menuWindowNewWindow.Activated += MenuWindowNewWindow_Activated;
                _menuWindowTile.Activated += MenuWindowTile_Activated;
                _menuWindowCascade.Activated += MenuWindowCascade_Activated;
                _menuWindowArrangeAll.Activated += MenuWindowArrangeAll_Activated;
                _menuWindowHide.Activated += MenuWindowHide_Activated;
                _menuWindowShow.Activated += MenuWindowShow_Activated;
                _menuHelpContents.Activated += MenuHelpContents_Activated;
                _menuHelpIndex.Activated += MenuHelpIndex_Activated;
                _menuHelpOnlineHelp.Activated += MenuHelpOnlineHelp_Activated;
                _menuHelpLicenseInformation.Activated += MenuHelpLicenseInformation_Activated;
                _menuHelpCheckForUpdates.Activated += MenuHelpCheckForUpdates_Activated;
                _menuHelpAbout.Activated += MenuHelpAbout_Activated;

                _tbFileNew.Clicked += ToolbarFileNew_Clicked;
                _tbFileOpen.Clicked += ToolbarFileOpen_Clicked;
                _tbFileSave.Clicked += ToolbarFileSave_Clicked;
                _tbFileSaveAs.Clicked += ToolbarFileSaveAs_Clicked;
                _tbFilePrint.Clicked += ToolbarFilePrint_Clicked;
                _tbEditUndo.Clicked += ToolbarEditUndo_Clicked;
                _tbEditRedo.Clicked += ToolbarEditRedo_Clicked;
                _tbEditCut.Clicked += ToolbarEditCut_Clicked;
                _tbEditCopy.Clicked += ToolbarEditCopy_Clicked;
                _tbEditPaste.Clicked += ToolbarEditPaste_Clicked;
                _tbEditDelete.Clicked += ToolbarEditDelete_Clicked;
                _tbEditFind.Clicked += ToolbarEditFind_Clicked;
                _tbEditReplace.Clicked += ToolbarEditReplace_Clicked;
                _tbEditRefresh.Clicked += ToolbarEditRefresh_Clicked;
                _tbEditPreferences.Clicked += ToolbarEditPreferences_Clicked;
                _tbHelpContents.Clicked += ToolbarHelpContents_Clicked;

                _buttonColor.Clicked += ButtonColor_Clicked;
                _buttonFont.Clicked += ButtonFont_Clicked;
                _txtSomeInt.Changed +=  SomeInt_Changed;
                _txtSomeOtherInt.Changed +=  SomeOtherInt_Changed;
                _txtStillAnotherInt.Changed +=  StillAnotherInt_Changed;
                _txtSomeString.Changed +=  SomeString_Changed;
                _txtSomeOtherString.Changed +=  SomeOtherString_Changed;
                _txtStillAnotherString.Changed +=  StillAnotherString_Changed;
                _chkSomeBoolean.Toggled +=  ChkSomeBoolean_Toggled;
                _chkSomeOtherBoolean.Toggled +=  ChkSomeOtherBoolean_Toggled;
                _chkStillAnotherBoolean.Toggled +=  ChkStillAnotherBoolean_Toggled;
                _cmdRun.Clicked += CmdRun_Clicked;
                #endregion Events

                //Not shown in titlebar, but visible in Alt-Tab
                SetIconFromFile("./Resources/App.png");

                _statusMessage.Text = "";
                _errorMessage.Text = "";

                ////(re)define default output delegate
                //ConsoleApplication.defaultOutputDelegate = ConsoleApplication.messageBoxWrapperOutputDelegate;

                //subscribe to view's notifications
                if (PropertyChanged != null)
                {
                    PropertyChanged += PropertyChangedEventHandlerDelegate;
                }

                InitViewModel();

                BindSizeAndLocation();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }
        #endregion Constructors

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion INotifyPropertyChanged

        #region Properties
        private string _ViewName = Program.APP_NAME;
        public string ViewName
        {
            get { return _ViewName; }
            set
            {
                _ViewName = value;
                OnPropertyChanged(nameof(ViewName));
            }
        }
        #endregion Properties

        #region Event Handlers

        #region PropertyChangedEventHandlerDelegates
        /// <summary>
        /// Note: model property changes update UI manually.
        /// Note: handle settings property changes manually.
        /// Note: because settings properties are a subset of the model
        ///  (every settings property should be in the model,
        ///  but not every model property is persisted to settings)
        ///  it is decided that for now the settings handler will
        ///  invoke the model handler as well.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">PropertyChangedEventArgs</param>
        protected void PropertyChangedEventHandlerDelegate
        (
            object sender,
            PropertyChangedEventArgs e
        )
        {
            try
            {
                #region Model
                if (e.PropertyName == "IsChanged")
                {
                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", e.PropertyName));
                    ApplySettings();
                }
                //Status Bar
                else if (e.PropertyName == "ActionIconIsVisible")
                {
                    _pictureAction.Visible = ViewModel.ActionIconIsVisible;
                }
                else if (e.PropertyName == "ActionIconImage")
                {
                    _pictureAction.IconName = (ViewModel?.ActionIconImage);
                }
                else if (e.PropertyName == "StatusMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _statusMessage.Text = (ViewModel?.StatusMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", StatusMessage));
                }
                else if (e.PropertyName == "ErrorMessage")
                {
                    //replace default action by setting control property
                    //skip status message updates after Viewmodel is null
                    _errorMessage.Text = (ViewModel?.ErrorMessage);
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "CustomMessage")
                {
                    //replace default action by setting control property
                    //StatusBarCustomMessage.Text = ViewModel.CustomMessage;
                    //e = new PropertyChangedEventArgs(e.PropertyName + ".handled");

                    //ConsoleApplication.defaultOutputDelegate(string.Format("{0}", ErrorMessage));
                }
                else if (e.PropertyName == "ErrorMessageToolTipText")
                {
                    _errorMessage.TooltipText = ViewModel.ErrorMessageToolTipText;
                }
                else if (e.PropertyName == "ProgressBarValue")
                {
                    _progressBar.Fraction = ViewModel.ProgressBarValue / 100;
                }
                else if (e.PropertyName == "ProgressBarMaximum")
                {
                    //TODO:_progressBar.Maximum = ViewModel.ProgressBarMaximum;
                }
                else if (e.PropertyName == "ProgressBarMinimum")
                {
                    //TODO:_progressBar.Minimum = ViewModel.ProgressBarMinimum;
                }
                else if (e.PropertyName == "ProgressBarStep")
                {
                    _progressBar.PulseStep = ViewModel.ProgressBarStep;
                }
                else if (e.PropertyName == "ProgressBarIsMarquee")
                {
                    //TODO:_progressBar.Style = (ViewModel.ProgressBarIsMarquee ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks);
                }
                else if (e.PropertyName == "ProgressBarIsVisible")
                {
                    if (_progressBar.Visible)
                    {
                        _progressBar.Pulse();
                    }
                    _progressBar.Visible = ViewModel.ProgressBarIsVisible;
                    if (_progressBar.Visible)
                    {
                        _progressBar.Pulse();
                    }
                }
                else if (e.PropertyName == "DirtyIconIsVisible")
                {
                    _pictureDirty.Visible = ViewModel.DirtyIconIsVisible;
                }
                else if (e.PropertyName == "DirtyIconImage")
                {
                    _pictureDirty.IconName = ViewModel.DirtyIconImage;
                }
                //use if properties cannot be databound
                else if (e.PropertyName == "SomeInt")
                {
                   _txtSomeInt.Text = ModelController<MVCModel>.Model.SomeInt.ToString();
                }
                else if (e.PropertyName == "SomeBoolean")
                {
                   _chkSomeBoolean.Active = ModelController<MVCModel>.Model.SomeBoolean;
                }
                else if (e.PropertyName == "SomeString")
                {
                   _txtSomeString.Text = ModelController<MVCModel>.Model.SomeString;
                }
                else if (e.PropertyName == "StillAnotherInt")
                {
                   _txtStillAnotherInt.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt.ToString();
                }
                else if (e.PropertyName == "StillAnotherBoolean")
                {
                   _chkStillAnotherBoolean.Active = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean;
                }
                else if (e.PropertyName == "StillAnotherString")
                {
                   _txtStillAnotherString.Text = ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString;
                }
                else if (e.PropertyName == "SomeOtherInt")
                {
                   _txtSomeOtherInt.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt.ToString();
                }
                else if (e.PropertyName == "SomeOtherBoolean")
                {
                   _chkSomeOtherBoolean.Active = ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean;
                }
                else if (e.PropertyName == "SomeOtherString")
                {
                   _txtSomeOtherString.Text = ModelController<MVCModel>.Model.SomeComponent.SomeOtherString;
                }
                //else if (e.PropertyName == "SomeComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("SomeComponent: {0},{1},{2}", ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt, ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean, ModelController<MVCModel>.Model.SomeComponent.SomeOtherString));
                //}
                //else if (e.PropertyName == "StillAnotherComponent")
                //{
                //    ConsoleApplication.defaultOutputDelegate(string.Format("StillAnotherComponent: {0},{1},{2}", ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean, ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString));
                //}
                else
                {
                    #if DEBUG_MODEL_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Model

                #region Settings
                if (e.PropertyName == "Dirty")
                {
                    //apply settings that don't have databindings
                    ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                }
                else
                {
                    #if DEBUG_SETTINGS_PROPERTYCHANGED
                    ConsoleApplication.defaultOutputDelegate(string.Format("e.PropertyName: {0}", e.PropertyName));
                    #endif
                }
                #endregion Settings
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        #endregion PropertyChangedEventHandlerDelegates

        #region Form Events
        /// <summary>
        /// View_Load
        /// </summary>
        private void Window_Realized(object sender, EventArgs e)
        {
            try
            {
                ViewModel.StatusMessage = string.Format("{0} starting...", ViewName);

                ViewModel.StatusMessage = string.Format("{0} started.", ViewName);

                // _Run();//only called here in console apps; use button in forms apps
            }
            catch (Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = string.Empty;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// handles Window_Delete and (indirectly) FileQuit_Clicked
        /// </summary>
        private void Window_DeleteEvent(object sender, DeleteEventArgs e)
        {
            bool resultDontQuit = false;

            try
            {
                ViewModel.FileExit(ref resultDontQuit);
                e.RetVal = resultDontQuit;
                if (!resultDontQuit)
                {
                    ViewModel.StatusMessage = string.Format("{0} completing...", ViewName);
                    DisposeSettings();
                    ViewModel.StatusMessage = string.Format("{0} completed.", ViewName);

                    ViewModel = null;

                    Application.Quit();
                }
            }
            catch(Exception ex)
            {
                ViewModel.ErrorMessage = ex.Message;
                ViewModel.StatusMessage = "";

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// TODO:handle ProcessCmdKey
        /// </summary>
        // [ GLib.ConnectBefore ] // need this to allow program to intercept the key first.
        private void Window_KeyPressEvent(object sender, KeyPressEventArgs e)
        {
        //     bool returnValue = default(bool);
            try
            {

                // Implement the Escape / Cancel keystroke
                // if (keyData == Keys.Cancel || keyData == Keys.Escape)
                // {
                //     //if a long-running cancellable-action has registered 
                //     //an escapable-event, trigger it
                //     InvokeActionCancel();

                //     // This keystroke was handled, 
                //     //don't pass to the control with the focus
                //     returnValue = true;
                // }
                // else
                // {
                //     returnValue = base.ProcessCmdKey(ref msg, keyData);
                // }

            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        //     return returnValue;
        }

        #endregion Form Events

        #region Control Events
        // private void cmdRun_Click(object sender, EventArgs e)
        // {
        //     ViewModel.DoSomething();
        // }

        private void ButtonColor_Clicked(object sender, EventArgs e)
        {
            ViewModel.GetColor();
        }

        private void ButtonFont_Clicked(object sender, EventArgs e)
        {
            ViewModel.GetFont();
        }

        private void SomeInt_Changed(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.SomeInt =
				int.TryParse(_txtSomeInt.Text, out int result) ? result : 0;
		}

        private void SomeOtherInt_Changed(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.SomeComponent.SomeOtherInt =
				int.TryParse(_txtSomeOtherInt.Text, out int result) ? result : 0;
		}

        private void StillAnotherInt_Changed(object sender, EventArgs e)
        {
			ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherInt =
				int.TryParse(_txtStillAnotherInt.Text, out int result) ? result : 0;
		}

        private void SomeString_Changed(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeString = _txtSomeString.Text;
        }

        private void SomeOtherString_Changed(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherString = _txtSomeOtherString.Text;
        }

        private void StillAnotherString_Changed(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherString = _txtStillAnotherString.Text;
        }

        private void ChkSomeBoolean_Toggled(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeBoolean = _chkSomeBoolean.Active;
        }

        private void ChkSomeOtherBoolean_Toggled(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.SomeComponent.SomeOtherBoolean = _chkSomeOtherBoolean.Active;
        }

        private void ChkStillAnotherBoolean_Toggled(object sender, EventArgs e)
        {
            ModelController<MVCModel>.Model.StillAnotherComponent.StillAnotherBoolean = _chkStillAnotherBoolean.Active;
        }

        private void CmdRun_Clicked(object sender, EventArgs e)
        {
            //do something
            ViewModel.DoSomething();
        }

        #endregion Control Events

        #region Menu Events

        private void MenuFileNew_Activated(object sender, EventArgs e)
        {
            ViewModel.FileNew();
        }

        private void MenuFileOpen_Activated(object sender, EventArgs e)
        {
            ViewModel.FileOpen();
        }

        private void MenuFileSave_Activated(object sender, EventArgs e)
        {
            ViewModel.FileSave();
        }

        private void MenuFileSaveAs_Activated(object sender, EventArgs e)
        {
            ViewModel.FileSaveAs();
        }

        private void MenuFilePrintActivated(object sender, EventArgs e)
        {
            ViewModel.FilePrint();
        }

        private void MenuFilePrintPreview_Activated(object sender, EventArgs e)
        {
            ViewModel.FilePrintPreview();
        }

        /// <summary>
        /// triggers Window_Delete which handles prompting user,
        /// and cancelling or quitting
        /// </summary>
        private void MenuFileQuit_Activated(object sender, EventArgs e)
        {
            Close();
        }

        private void MenuEditUndo_Activated(object sender, EventArgs e)
        {
            ViewModel.EditUndo();
        }

        private  void MenuEditRedo_Activated(object sender, EventArgs e)
        {
            ViewModel.EditRedo();
        }

        private  void MenuEditSelectAll_Activated(object sender, EventArgs e)
        {
            ViewModel.EditSelectAll();
        }

        private  void MenuEditCut_Activated(object sender, EventArgs e)
        {
            ViewModel.EditCut();
        }

        private  void MenuEditCopyActivated(object sender, EventArgs e)
        {
            ViewModel.EditCopy();
        }

        private  void MenuEditPaste_Activated(object sender, EventArgs e)
        {
            ViewModel.EditPaste();
        }

        private  void MenuEditPasteSpecial_Activated(object sender, EventArgs e)
        {
            ViewModel.EditPasteSpecial();
        }

        private  void MenuEditDelete_Activated(object sender, EventArgs e)
        {
            ViewModel.EditDelete();
        }

        private  void MenuEditFind_Activated(object sender, EventArgs e)
        {
            ViewModel.EditFind();
        }

        private  void MenuEditReplace_Activated(object sender, EventArgs e)
        {
            ViewModel.EditReplace();
        }

        private  void MenuEditRefresh_Activated(object sender, EventArgs e)
        {
            ViewModel.EditRefresh();
        }

        private void MenuEditProperties_Activated(object sender, EventArgs e)
		{
            ViewModel.EditProperties();
        }

        private void MenuEditPreferences_Activated(object sender, EventArgs e)
        {
            ViewModel.EditPreferences();
        }
        private void MenuWindowNewWindow_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowNewWindow();
        }
        private void MenuWindowTile_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowTile();
        }
        private void MenuWindowCascade_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowCascade();
        }
        private void MenuWindowArrangeAll_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowArrangeAll();
        }
        private void MenuWindowHide_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowHide();
        }
        private void MenuWindowShow_Activated(object sender, EventArgs e)
		{
            ViewModel.WindowShow();
        }
        private void MenuHelpContents_Activated(object sender, EventArgs e)
		{
            ViewModel.HelpContents();
        }
        private void MenuHelpIndex_Activated(object sender, EventArgs e)
		{
            ViewModel.HelpIndex();
        }
        private void MenuHelpOnlineHelp_Activated(object sender, EventArgs e)
		{
            ViewModel.HelpOnlineHelp();
        }
        private void MenuHelpLicenseInformation_Activated(object sender, EventArgs e)
		{
            ViewModel.HelpLicenceInformation();
        }
        private void MenuHelpCheckForUpdates_Activated(object sender, EventArgs e)
		{
            ViewModel.HelpCheckForUpdates();
        }
        private void MenuHelpAbout_Activated(object sender, EventArgs e)
        {
            ViewModel.HelpAbout<AssemblyInfo>();
        }
        #endregion Menu Events

        #region Toolbar Events
        private  void ToolbarFileNew_Clicked(object sender, EventArgs e)
        {
            ViewModel.FileNew();
        }
        private void ToolbarFileOpen_Clicked(object sender, EventArgs e)
        {
            ViewModel.FileOpen();
        }
        private void ToolbarFileSave_Clicked(object sender, EventArgs e)
        {
            ViewModel.FileSave();
        }
        private void ToolbarFileSaveAs_Clicked(object sender, EventArgs e)
        {
            ViewModel.FileSaveAs();
        }
        private void ToolbarFilePrint_Clicked(object sender, EventArgs e)
        {
            ViewModel.FilePrint();
        }
        private  void ToolbarEditUndo_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditUndo();
        }
        private  void ToolbarEditRedo_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditRedo();
        }
        private  void ToolbarEditCut_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditCut();
        }
        private  void ToolbarEditCopy_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditCopy();
        }
        private  void ToolbarEditPaste_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditPaste();
        }
        private  void ToolbarEditDelete_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditDelete();
        }
        private  void ToolbarEditFind_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditFind();
        }
        private  void ToolbarEditReplace_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditReplace();
        }
        private  void ToolbarEditRefresh_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditRefresh();
        }
        private void ToolbarEditPreferences_Clicked(object sender, EventArgs e)
        {
            ViewModel.EditPreferences();
        }
        private  void ToolbarHelpContents_Clicked(object sender, EventArgs e)
        {
            ViewModel.HelpContents();
        }
        #endregion Toolbar Events

        #endregion Event Handlers

        #region Methods
        #region FormAppBase
        protected void InitViewModel()
        {
            FileDialogInfo<Window, ResponseType> settingsFileDialogInfo = null;

            try
            {
                //tell controller how model should notify view about non-persisted properties AND including model properties that may be part of settings
                ModelController<MVCModel>.DefaultHandler = PropertyChangedEventHandlerDelegate;

                //tell controller how settings should notify view about persisted properties
                SettingsController<MVCSettings>.DefaultHandler = PropertyChangedEventHandlerDelegate;

				InitModelAndSettings();

				//settings used with file dialog interactions
				settingsFileDialogInfo =
					new FileDialogInfo<Window, ResponseType>
					(
						parent: this,
						modal: true,
						title: null,
						response: ResponseType.None,
						newFilename: SettingsController<MVCSettings>.FILE_NEW,
						filename: null,
						extension: SettingsBase.FileTypeExtension,
						description: SettingsBase.FileTypeDescription,
						typeName: SettingsBase.FileTypeName,
						additionalFilters:
						[
							"MvcSettings files (*.mvcsettings)|*.mvcsettings",
							"JSON files (*.json)|*.json",
							"XML files (*.xml)|*.xml",
							"All files (*.*)|*.*"
						],
						multiselect: false,
						initialDirectory: default,
						forceDialog: false,
						forceNew: false,
						customInitialDirectory: Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
					)
					{
						//set dialog caption
						Title = Title
					};

				//class to handle standard behaviors
				ViewModelController<string, MVCViewModel>.New
                (
                    ViewName,
                    new MVCViewModel
                    (
                        PropertyChangedEventHandlerDelegate,
                        new Dictionary<string, string>()
                        { //TODO:ideally, should get these from library, but items added did not get generated into resource class.
                            { "New", Stock.New },
                            { "Open", Stock.Open },
                            { "Save", Stock.Save },
                            { "SaveAs", Stock.SaveAs },
                            { "Print", Stock.Print },
                            { "PrintPreview", Stock.PrintPreview },
                            { "Quit", Stock.Quit },
                            { "Undo", Stock.Undo },
                            { "Redo", Stock.Redo },
                            { "SelectAll", Stock.SelectAll },
                            { "Cut", Stock.Cut },
                            { "Copy", Stock.Copy },
                            { "Paste", Stock.Paste },
                            { "Delete", Stock.Delete },
                            { "Find", Stock.Find },
                            { "Replace", Stock.FindAndReplace },
                            { "Refresh", Stock.Refresh },
                            { "Preferences", Stock.Preferences },
                            { "Properties", Stock.Properties },
                            { "Contents", Stock.Help },
                            { "About", Stock.About }
                        },
                        settingsFileDialogInfo
                    )
                );

                //select a viewModel by view name
                ViewModel = ViewModelController<string, MVCViewModel>.ViewModel[ViewName];

                // BindFormUi();

                //Init config parameters
                if (!LoadParameters())
                {
                    throw new Exception(string.Format("Unable to load config file parameter(s)."));
                }

                //Load
                if ((SettingsController<MVCSettings>.FilePath == null) || SettingsController<MVCSettings>.Filename.StartsWith(SettingsController<MVCSettings>.FILE_NEW))
                {
                    //NEW
                    ViewModel.FileNew();
                }
                else
                {
                    //OPEN
                    ViewModel.FileOpen(false);
                }

#if debug
            //debug view
            menuEditProperties_Click(sender, e);
#endif

                //Display dirty state
                ModelController<MVCModel>.Model.Refresh();
            }
            catch (Exception ex)
            {
                if (ViewModel != null)
                {
                    ViewModel.ErrorMessage = ex.Message;
                }
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        protected static void InitModelAndSettings()
        {
            //create Settings before first use by Model
            if (SettingsController<MVCSettings>.Settings == null)
            {
                SettingsController<MVCSettings>.New();
            }
            //Model properties rely on Settings, so don't call Refresh before this is run.
            if (ModelController<MVCModel>.Model == null)
            {
                ModelController<MVCModel>.New();
            }
        }

        protected void DisposeSettings()
        {
            MessageDialogInfo<Window, ResponseType, DialogFlags, MessageType, ButtonsType> questionMessageDialogInfo = null;
            string errorMessage = null;

            //save user and application settings 
            Properties.Settings.Default.Save();

            if (SettingsController<MVCSettings>.Settings.Dirty)
            {
                //prompt before saving
                questionMessageDialogInfo = new MessageDialogInfo<Window, ResponseType, DialogFlags, MessageType, ButtonsType>
                (
                    parent: this,
                    modal: true,
                    title: Title,
                    dialogFlags: DialogFlags.DestroyWithParent,
                    messageType: MessageType.Question,
                    buttonsType: ButtonsType.YesNo,
                    message: "Save changes?",
                    response: ResponseType.None
                );
                if (!Dialogs.ShowMessageDialog(ref questionMessageDialogInfo, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                // DialogResult dialogResult = MessageBox.Show("Save changes?", this.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (questionMessageDialogInfo.Response)
                {
					case ResponseType.Yes:
						//SAVE
						ViewModel.FileSave();

						break;

					case ResponseType.No:
						break;

					default:
						throw new InvalidEnumArgumentException();
				}
            }

            //unsubscribe from model notifications
            ModelController<MVCModel>.Model.PropertyChanged -= PropertyChangedEventHandlerDelegate;
        }

        protected static void Run()
        {
            //MessageBox.Show("running", "MVC", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
        #endregion FormAppBase

        #region Utility
        // /// <summary>
        // /// Bind static Model controls to Model Controller
        // /// </summary>
        // private void BindFormUi()
        // {
        //     try
        //     {
        //         //Form

        //         //Controls
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// <summary>
        // /// Bind Model controls to Model Controller
        // /// Note: databinding not available in GtkSharp, but leave this in place 
        // ///  in case I figure out a way to do this
        // /// </summary>
        // private void BindModelUi()
        // {
        //     try
        //     {
		// 		BindField(_txtSomeInt, ModelController<MVCModel>.Model, "SomeInt");
		// 		BindField(_txtSomeString, ModelController<MVCModel>.Model, "SomeString");
		// 		BindField(_chkSomeBoolean, ModelController<MVCModel>.Model, "SomeBoolean", "Checked");

		// 		BindField(_txtSomeOtherInt, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherInt");
		// 		BindField(_txtSomeOtherString, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherString");
		// 		BindField(_chkSomeOtherBoolean, ModelController<MVCModel>.Model, "SomeComponent.SomeOtherBoolean", "Checked");

		// 		BindField(_txtStillAnotherInt, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherInt");
		// 		BindField(_txtStillAnotherString, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherString");
		// 		BindField(_chkStillAnotherBoolean, ModelController<MVCModel>.Model, "StillAnotherComponent.StillAnotherBoolean", "Checked");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //         throw;
        //     }
        // }

        // /// Note: databinding not available in GtkSharp, but leave this in place 
        // ///  in case I figure out a way to do this
        // private void BindField<TControl, TModel>
        // (
        //     TControl fieldControl,
        //     TModel model,
        //     string modelPropertyName,
        //     string controlPropertyName = "Text",
        //     bool formattingEnabled = false,
        //     //DataSourceUpdateMode dataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged,
        //     bool reBind = true
        // )
        //     where TControl : Gtk.Widget
        // {
        //     try
        //     {
        //         //TODO: .RemoveSignalHandler ?
        //         //fieldControl.DataBindings.Clear();
        //         if (reBind)
        //         {
        //             //TODO:.AddSignalHandler ?
        //             //fieldControl.DataBindings.Add(controlPropertyName, model, modelPropertyName, formattingEnabled, dataSourceUpdateMode);
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Apply Settings to viewer.
        /// </summary>
        private void  ApplySettings()
        {
            try
            {
                // _ValueChangedProgrammatically = true;

                //apply settings that have databindings
                // BindModelUi();

                //apply settings that shouldn't use databindings

                //apply settings that can't use databindings
                Title = System.IO.Path.GetFileName(SettingsController<MVCSettings>.Filename) + " - " + ViewName;

                //apply settings that don't have databindings
                ViewModel.DirtyIconIsVisible = SettingsController<MVCSettings>.Settings.Dirty;
                //update remaining non-bound controls: textboxes, checkboxes
                //Note: ModelController<MVCModel>.Model.Refresh() will cause SO here
                ModelController<MVCModel>.Model.Refresh("SomeInt");
                ModelController<MVCModel>.Model.Refresh("SomeBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeString");
                ModelController<MVCModel>.Model.Refresh("SomeOtherInt");
                ModelController<MVCModel>.Model.Refresh("SomeOtherBoolean");
                ModelController<MVCModel>.Model.Refresh("SomeOtherString");
                ModelController<MVCModel>.Model.Refresh("StillAnotherInt");
                ModelController<MVCModel>.Model.Refresh("StillAnotherBoolean");
                ModelController<MVCModel>.Model.Refresh("StillAnotherString");

                // _ValueChangedProgrammatically = false;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        // /// <summary>
        // /// Set function button and menu to enable value, and cancel button to opposite.
        // /// For now, do only disabling here and leave enabling based on biz logic 
        // ///  to be triggered by refresh?
        // /// </summary>
        // /// <param name="functionButton"></param>
        // /// <param name="functionMenu"></param>
        // /// <param name="cancelButton"></param>
        // /// <param name="enable"></param>
        // private void SetFunctionControlsEnable
        // (
        //     Button functionButton,
        //     ToolButton functionToolbarButton,
        //     ImageMenuItem functionMenu,
        //     Button cancelButton,
        //     bool enable
        // )
        // {
        //     try
        //     {
        //         //stand-alone button
        //         if (functionButton != null)
        //         {
        //             functionButton.Sensitive = enable;
        //         }

        //         //toolbar button
        //         if (functionToolbarButton != null)
        //         {
        //             functionToolbarButton.Sensitive = enable;
        //         }

        //         //menu item
        //         if (functionMenu != null)
        //         {
        //             functionMenu.Sensitive = enable;
        //         }

        //         //stand-alone cancel button
        //         if (cancelButton != null)
        //         {
        //             cancelButton.Sensitive = !enable;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Invoke any delegate that has been registered
        ///  to cancel a long-running background process.
        /// </summary>
        // private void InvokeActionCancel()
        // {
        //     try
        //     {
        //         //execute cancellation hook
        //         if (cancelDelegate != null)
        //         {
        //             cancelDelegate();
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
        //     }
        // }

        /// <summary>
        /// Load from app config; override with command line if present
        /// </summary>
        /// <returns>bool</returns>
        private static bool LoadParameters()
        {
            bool returnValue = default;

            try
            {
                // First, get configured values

                //get filename from App.config
                if (!Configuration.ReadString("SettingsFilename", out string _settingsFilename))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsFilename: {0}", "SettingsFilename"));
                }
                if ((_settingsFilename == null) || (_settingsFilename == SettingsController<MVCSettings>.FILE_NEW))
                {
                    throw new ApplicationException(string.Format("Settings filename not set: '{0}'.\nCheck SettingsFilename in App.config file.", _settingsFilename));
                }
                //use with the supplied path
                SettingsController<MVCSettings>.Filename = _settingsFilename;

                //identify directory to specified, or use default
                if (System.IO.Path.GetDirectoryName(_settingsFilename)?.Length == 0)
                {
                    //supply default path if missing
                    SettingsController<MVCSettings>.Pathname = Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator();
                }

                //get serialization format from App.config
                if (!Configuration.ReadString("SettingsSerializeAs", out string _settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Unable to load SettingsSerializeAs: {0}", "SettingsSerializeAs"));
                }
                if (string.IsNullOrEmpty(_settingsSerializeAs))
                {
                    throw new ApplicationException(string.Format("Settings serialization format not set: '{0}'.\nCheck SettingsSerializeAs in App.config file.", _settingsSerializeAs));
                }
                //use with the filename
                SettingsBase.SerializeAs = SettingsBase.ToSerializationFormat(_settingsSerializeAs);

                //Second, override with passed values

                if ((Program.Filename != default) && (Program.Filename != SettingsController<MVCSettings>.FILE_NEW))
                {
                    //got filename from command line
                    SettingsController<MVCSettings>.Filename = Program.Filename;
                }
                if (Program.Directory != default)
                {
                    //get default directory from command line
                    SettingsController<MVCSettings>.Pathname = Program.Directory.WithTrailingSeparator();
                }
                if (Program.Format != default)
                {
                    // get default format from command line
                    SettingsBase.SerializeAs = Program.Format switch
                    {
                        "xml" => SettingsBase.SerializationFormat.Xml,
                        "json" => SettingsBase.SerializationFormat.Json,
                        _ => default
                    };
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        private void BindSizeAndLocation()
        {
            //Note:Size must be done after InitializeComponent(); do Location this way as well.--SJS
            // this.DataBindings.Add(new System.Windows.Forms.Binding("Location", global::MVCForms.Properties.Settings.Default, "Location", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            // this.DataBindings.Add(new System.Windows.Forms.Binding("ClientSize", global::MVCForms.Properties.Settings.Default, "Size", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            SetDefaultSize(Properties.Settings.Default.Size.Width, Properties.Settings.Default.Size.Height);
            SetPosition(WindowPosition.Center); // global::MVCForms.Properties.Settings.Default.Location;
        }
        #endregion Utility
        #endregion Methods

        #region Actions
        // private async Task DoSomething()
        // {
        //     // await Task.Delay(3000); 
        //     for (int i = 0; i < 3; i++)
        //     {
        //         //TODO:DoEvents...
        //         await Task.Delay(1000); 
        //     }
        // }

        #endregion Actions

    }
}
